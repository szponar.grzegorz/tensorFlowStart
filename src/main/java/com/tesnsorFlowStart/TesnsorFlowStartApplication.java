package com.tesnsorFlowStart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesnsorFlowStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesnsorFlowStartApplication.class, args);
	}
}
